<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Portfolio_Theme
 */

get_header(); ?>
	<div class="article-hero-container">
		<div class="parallax-container" data-parallax="scroll" data-bleed="10" data-speed="0.2" data-image-src="<?php the_post_thumbnail_url(); ?>" data-natural-width="1920" data-natural-height="1080" style="height: 100vh;"></div>

		<div class="project-title-section">
				<?php the_title( '<h1 class="title-block project-title">', '</h1>' ); ?>
				<img class="project-featured-image" src="<?php echo MultiPostThumbnails::get_post_thumbnail_url(get_post_type(), 'project-main-image'); ?>" alt="">
		</div>
	</div>

	<section id="primary" class="container content-area">
		<main id="main" class="site-main">

		<?php

		while ( have_posts() ) : the_post(); ?>
			<h2 class="title">The Project</h2>

			<?php the_content();

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</section><!-- #primary -->
	<section class="container project-showcase">
		<img class="showcase-one" src="<?php echo MultiPostThumbnails::get_post_thumbnail_url(get_post_type(), 'project-showcase-one'); ?>" alt="" data-tilt="" data-tilt-max="0.5"/>
		<img class="showcase-two" src="<?php echo MultiPostThumbnails::get_post_thumbnail_url(get_post_type(), 'project-showcase-one'); ?>" alt="" data-tilt="" data-tilt-max="0.5"/>
		<div class="mobile-container">
			<img class="showcase-mobile" src="<?php echo MultiPostThumbnails::get_post_thumbnail_url(get_post_type(), 'project-showcase-mobile'); ?>" alt="" />
		</div>
	</section>

	<div class="clear"></div>
	
	<section class="container">
		<div class="outcome-container">
			<h2 class="title">The Outcome</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In efficitur quam sed diam mollis consequat. Nam ac ornare neque, vel scelerisque enim. Proin massa nisl, accumsan sit amet ligula eu, luctus ullamcorper magna. Etiam at sapien vulputate, accumsan ex id, rutrum lacus. In et metus vel libero vulputate pulvinar id ut justo. Donec pharetra et mauris ut condimentum. Ut non ex pulvinar quam ullamcorper consectetur. Etiam viverra pellentesque molestie. </p>
		</div>
	</section>

	<section class="post-navigation">
		<?php

			$related = get_posts( array(
				'category__in' => wp_get_post_categories($post->ID),
				'numberposts' => 2,
				'post_type' => 'projects',
				'post__not_in' => array($post->ID) ) );

			if( $related ) foreach( $related as $post ) {
			setup_postdata($post); ?>

				<a href="<?php the_permalink() ?>">
					<div class="post-container" style="background-image: url('<?php echo get_the_post_thumbnail_url();?>');">
						<h4><?php the_title(); ?></h4>
					</div>
					<div class="overlay"></div>
				</a>

			<?php }

			wp_reset_postdata();
		?>
	</section>



<?php
get_footer();
