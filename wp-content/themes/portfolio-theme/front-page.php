<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Portfolio_Theme
 */

get_header(); ?>
	<div class="parallax-container" data-parallax="scroll" data-bleed="10" data-speed="0.2" data-image-src="<?php echo get_theme_mod('homepage_hero_image', get_bloginfo('template_url').'inc/img/body-top.jpg'); ?>" data-natural-width="1920" data-natural-height="1080" style="height: 100vh;"></div>

	<section class="container profile-section">
		<div class="title-block">
			<h2><?php echo get_theme_mod('profile_heading'); ?></h2>
			<span><?php echo get_theme_mod('profile_subheading'); ?></span>
		</div>
		<div class="row">
			<div class="col profile-stats">
				<div class="profile-item">
					<span class="profile-title">Profile</span>
					<span class="profile-content"><?php echo get_theme_mod('profile_profession'); ?></span>
				</div>
				<div class="profile-item">
					<span class="profile-title">Skills</span>
					<section class="profile-content" id="profile-skills">
						<div class="skill-item">
							<progress value="80" max="100"></progress><span>JavaScript/jQuery</span>
						</div>
						<div class="skill-item">
							<progress value="70" max="100"></progress><span>HTML5/CSS3</span>
						</div>
						<div class="skill-item">
							<progress value="80" max="100"></progress><span>NodeJS</span>
						</div>
						<div class="skill-item">
							<progress value="70" max="100"></progress><span>Java/PHP</span>
						</div>
					</section>
				</div>
				<div class="profile-item">
					<span class="profile-title">Contact</span>
					<div class="profile-content">
						<a href="mailto:ben.seward@googlemail.com">ben.seward@googlemail.com</a>
					</div>
				</div>
			</div>
			<div class="col">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post();
					the_content();
				endwhile; else: ?>
					<p>Sorry, no posts matched your criteria.</p>
				<?php endif; ?>
			</div>
		</div>
	</section>

	<section class="container project-preview">
		<div class="title-block">
			<h2>Recent Work</h2>
			<span>Check out a few of the projects I've been working on recently.</span>
		</div>

		 <?php
		 $args = array(
		'post_type' => 'projects', // Your custom post type
		'posts_per_page' => '8', // Change the number to whatever you wish
		'order_by' => 'date', // Some optional sorting
		'order' => 'ASC',
		);
		$new_query = new WP_Query ($args);
		if ($new_query->have_posts()) {
		    while($new_query->have_posts()){
		        $new_query->the_post(); ?>
						<div class="project-preview-item row">
							<div class="project-inner-container">
								<a href="<?php echo esc_url( get_permalink() ); ?>" class="col-md-4 project-item-bg" style="background-image: url('<?php the_post_thumbnail_url(); ?>');">
									<h3 class="hidden-title"><?php the_title(); ?></h3>
								</a>
								<div class="col-md-8 project-item-content">
									<h3><?php the_title(); ?></h3>
									<?php the_content(); ?>
									<a class="std-button" href="<?php echo esc_url( get_permalink() ); ?>"><?php esc_html_e( 'View Project', 'textdomain' ); ?></a>
								</div>
							</div>
						</div>
		        <?php
						// Get a list of post's categories
		        $categories = get_the_category($post->ID);
		        foreach ($categories as $category) {
		            echo $category->name;
		        }
		    }
		}
		wp_reset_postdata();
		  ?>
	</section>

<?php
get_footer();
