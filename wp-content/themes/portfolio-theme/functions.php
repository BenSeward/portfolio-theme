<?php
/**
 * Portfolio Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Portfolio_Theme
 */

if ( ! function_exists( 'portfolio_theme_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function portfolio_theme_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Portfolio Theme, use a find and replace
		 * to change 'portfolio-theme' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'portfolio-theme', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'portfolio-theme' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'portfolio_theme_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'portfolio_theme_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function portfolio_theme_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'portfolio_theme_content_width', 640 );
}
add_action( 'after_setup_theme', 'portfolio_theme_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function portfolio_theme_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'portfolio-theme' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'portfolio-theme' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'portfolio_theme_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function portfolio_theme_scripts() {
	wp_enqueue_style( 'portfolio-theme-style', get_stylesheet_uri() );

	wp_enqueue_style( 'font-awesine', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' );

	wp_enqueue_script( 'portfolio-theme-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'portfolio-theme-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	wp_enqueue_script( 'parallax-image', get_template_directory_uri() . '/js/parallax.min.js', array('jquery'), '20151215', true );

	wp_enqueue_script( 'tilt-image', get_template_directory_uri() . '/js/tilt.jquery.js', array('jquery'), '1.1', true );

	// Include custom scripts
	wp_enqueue_script( 'custom-scripts', get_template_directory_uri() . '/js/scripts.js', array('jquery'), '1.1', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'portfolio_theme_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/*
* Creating a function to create our CPT
*/

function project_post_type() {

// Set UI labels for Custom Post Type
    $labels = array(
        'name'                => _x( 'Projects', 'text_domain'),
        'singular_name'       => _x( 'Project', 'text_domain'),
        'menu_name'           => __( 'Projects', 'text_domain'),
        'parent_item_colon'   => __( 'Parent Project', 'text_domain'),
        'all_items'           => __( 'All Projects', 'text_domain'),
        'view_item'           => __( 'View Project', 'text_domain'),
        'add_new_item'        => __( 'Add New Project', 'text_domain'),
        'add_new'             => __( 'Add New', 'text_domain'),
        'edit_item'           => __( 'Edit Project', 'text_domain'),
        'update_item'         => __( 'Update Project', 'text_domain'),
        'search_items'        => __( 'Search Projects', 'text_domain'),
        'not_found'           => __( 'Not Found', 'text_domain'),
        'not_found_in_trash'  => __( 'Not found in Trash', 'text_domain'),
    );

// Set other options for Custom Post Type

    $args = array(
        'label'               => __( 'projects'),
        'description'         => __( 'Projects I have worked on'),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
        // You can associate this CPT with a taxonomy or custom taxonomy.
        'taxonomies'          => array( 'projects' ),
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'page',
    );

    // Registering your Custom Post Type
    register_post_type( 'projects', $args );

}

/* Hook into the 'init' action so that the function
* Containing our post type registration is not
* unnecessarily executed.
*/

add_action( 'init', 'project_post_type', 0 );


//Support for multiple featured images on portfolio project pages
if (class_exists('MultiPostThumbnails')) {
		// Project Background Hero Image
		new MultiPostThumbnails(
        array(
            'label' => 'Project Main Image',
            'id' => 'project-main-image',
            'post_type' => 'projects'
        )
    );
		// First Project Showcase Image
		new MultiPostThumbnails(
        array(
            'label' => 'Project Showcase One',
            'id' => 'project-showcase-one',
            'post_type' => 'projects'
        )
    );
		// Second Project Showcase Image
		new MultiPostThumbnails(
        array(
            'label' => 'Project Showcase Two',
            'id' => 'project-showcase-two',
            'post_type' => 'projects'
        )
    );
		// Mobile Project Showcase Image
		new MultiPostThumbnails(
        array(
            'label' => 'Project Showcase Mobile',
            'id' => 'project-showcase-mobile',
            'post_type' => 'projects'
        )
    );
}
