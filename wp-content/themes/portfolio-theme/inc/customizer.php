<?php
/**
 * Portfolio Theme Theme Customizer
 *
 * @package Portfolio_Theme
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function portfolio_theme_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial( 'blogname', array(
			'selector'        => '.site-title a',
			'render_callback' => 'portfolio_theme_customize_partial_blogname',
		) );
		$wp_customize->selective_refresh->add_partial( 'blogdescription', array(
			'selector'        => '.site-description',
			'render_callback' => 'portfolio_theme_customize_partial_blogdescription',
		) );
	}
}
add_action( 'customize_register', 'portfolio_theme_customize_register' );

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function portfolio_theme_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function portfolio_theme_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function portfolio_theme_customize_preview_js() {
	wp_enqueue_script( 'portfolio-theme-customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'portfolio_theme_customize_preview_js' );


/**
 * Homepage Customizer Options.
 */

function homepage_customize_register($wp_customize) {
	// Create Homepage Options Section
	$wp_customize->add_section('homepage_options', array(
		'title'   => __('Homepage Options', 'homepageoptions'),
		'description' => sprintf(__('Options for homepage','homepageoptions')),
		'priority'    => 10
	));

	// Creating Hero Image Banner
	$wp_customize->add_setting('homepage_hero_image', array(
		'default'   => get_bloginfo('template_directory').'inc/img/body-top.jpg',
		'type'      => 'theme_mod'
	));
	$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'homepage_hero_image', array(
		'label'   => __('Homepage Hero Image', 'homepage_hero_image'),
		'section' => 'homepage_options',
		'settings' => 'homepage_hero_image',
		'priority'  => 1
	)));

	// Creating Section For Profile
	$wp_customize->add_section('homepage_profile_options', array(
		'title'   => __('Profile Options', 'profileoptions'),
		'description' => sprintf(__('Options for profile section of homepage','profileoptions')),
		'priority'    => 10
	));

	// Creating Profile Section Heading & Sub Heading
	$wp_customize->add_setting('profile_heading', array(
      'default'   => _x('Custom Bootstrap Wordpress Theme', 'profileoptions'),
      'type'      => 'theme_mod'
    ));
    $wp_customize->add_control('profile_heading', array(
      'label'   => __('Heading', 'profileoptions'),
      'section' => 'homepage_profile_options',
      'priority'  => 2
    ));

    $wp_customize->add_setting('profile_subheading', array(
      'default'   => _x('Sociis natoque penatibus et magnis dis parturient montes.', 'profileoptions'),
      'type'      => 'theme_mod'
    ));
    $wp_customize->add_control('profile_subheading', array(
      'label'   => __('Text', 'profileoptions'),
      'section' => 'homepage_profile_options',
      'priority'  => 3
    ));

		// Adding Profile title
		$wp_customize->add_setting('profile_profession', array(
			'default'   => _x('UX Designer  Developer', 'profileoptions'),
			'type'			=> 'theme_mod'
		));
		$wp_customize->add_control('profile_profession', array(
      'label'   => __('Text', 'profileoptions'),
      'section' => 'homepage_profile_options',
      'priority'  => 3
    ));

}

add_action('customize_register', 'homepage_customize_register');
